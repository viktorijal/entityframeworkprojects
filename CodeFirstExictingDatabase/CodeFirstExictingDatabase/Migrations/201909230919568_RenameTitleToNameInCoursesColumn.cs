namespace CodeFirstExictingDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameTitleToNameInCoursesColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Courses", "NameCourse", c => c.String(nullable: false));
            Sql("UPDATE Courses SET NameCourse = Title");
            DropColumn("dbo.Courses", "Title");
            // RenameColumn("dbo.Courses", "Title", "NameCourse");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Courses", "Title", c => c.String(nullable: false));
            Sql("UPDATE Courses SET Title = NameCourse");
            DropColumn("dbo.Courses", "NameCourse");
        }
    }
}
